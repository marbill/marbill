package org.henzenmann.marbill.common;

import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.graphics.g3d.utils.MeshBuilder;
import com.badlogic.gdx.graphics.g3d.utils.MeshPartBuilder;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;

public class MeshUtils {
	
	public static Mesh createBox(float width, float height, float depth,
			float r, float g, float b, float a)
	{
		MeshBuilder meshBuilder = new MeshBuilder();

		meshBuilder.begin(new VertexAttributes(new VertexAttribute(Usage.Position, 3, ShaderProgram.POSITION_ATTRIBUTE),
				new VertexAttribute(Usage.Color, 4, ShaderProgram.COLOR_ATTRIBUTE)),
				GL10.GL_TRIANGLES);

		meshBuilder.setColor(r, g, b, a);
		meshBuilder.box(width, height, depth);

		return meshBuilder.end();
	}

	public static Mesh createSprite(float w, float h) {

		MeshBuilder meshBuilder = new MeshBuilder();
		meshBuilder.begin( new VertexAttributes( new VertexAttribute(Usage.Position, 3, ShaderProgram.POSITION_ATTRIBUTE),
				                                 new VertexAttribute(Usage.TextureCoordinates, 2, ShaderProgram.TEXCOORD_ATTRIBUTE) ),
						   GL10.GL_TRIANGLES );
		
		MeshPartBuilder.VertexInfo v0 = new MeshPartBuilder.VertexInfo();
		v0.setPos(w * -0.5f,      0.5f, 0f);
		v0.setUV(0f, 0f);
				
		MeshPartBuilder.VertexInfo v1 = new MeshPartBuilder.VertexInfo(); 
		v1.setPos(w * -0.5f, h * -0.5f, 0f);
		v1.setUV(0f, 1f);

		MeshPartBuilder.VertexInfo v2 = new MeshPartBuilder.VertexInfo(); 
		v2.setPos(w *  0.5f, h * -0.5f, 0f);
		v2.setUV(1f, 1f);

		MeshPartBuilder.VertexInfo v3 = new MeshPartBuilder.VertexInfo(); 
		v3.setPos(w *  0.5f, h *  0.5f, 0f);
		v3.setUV(1f, 0f);
		
		meshBuilder.triangle(v0, v1, v2);
		meshBuilder.triangle(v2, v3, v0);
		
		return meshBuilder.end();
	}
	
	static public Mesh createArrowHead()
	{
		final float halfWidth = 0.7f;
		final float height = 1.2f;
		
		MeshBuilder meshBuilder = new MeshBuilder();

		meshBuilder.begin(new VertexAttributes(new VertexAttribute(Usage.Position, 3, ShaderProgram.POSITION_ATTRIBUTE)),
				GL10.GL_TRIANGLES);

		MeshPartBuilder.VertexInfo v0 = new MeshPartBuilder.VertexInfo();
		v0.setPos(        0f, 0f, -height);
		
		MeshPartBuilder.VertexInfo v1 = new MeshPartBuilder.VertexInfo();
		v1.setPos(-halfWidth, 0f,      0f);

		MeshPartBuilder.VertexInfo v2 = new MeshPartBuilder.VertexInfo();
		v2.setPos( halfWidth, 0f,      0f);
		
		meshBuilder.triangle(v0, v1, v2);
		
		return meshBuilder.end();
	}

	static public Mesh createArrowTrunk()
	{
		final float width = 0.8f;
		final float halfWidth = width * 0.5f;
		
		MeshBuilder meshBuilder = new MeshBuilder();

		meshBuilder.begin(new VertexAttributes(new VertexAttribute(Usage.Position, 3, ShaderProgram.POSITION_ATTRIBUTE)),
				GL10.GL_TRIANGLES);

		MeshPartBuilder.VertexInfo v0 = new MeshPartBuilder.VertexInfo();
		v0.setPos(-halfWidth, 0f, -0.5f);
		
		MeshPartBuilder.VertexInfo v1 = new MeshPartBuilder.VertexInfo();
		v1.setPos(-halfWidth, 0f,  0.5f);

		MeshPartBuilder.VertexInfo v2 = new MeshPartBuilder.VertexInfo();
		v2.setPos( halfWidth, 0f,  0.5f);
		
		MeshPartBuilder.VertexInfo v3 = new MeshPartBuilder.VertexInfo();
		v3.setPos( halfWidth, 0f, -0.5f);

		meshBuilder.triangle(v0, v1, v2);
		meshBuilder.triangle(v2, v3, v0);
		
		return meshBuilder.end();
	}
	
} // class
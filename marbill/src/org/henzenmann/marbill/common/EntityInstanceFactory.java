package org.henzenmann.marbill.common;

import java.util.HashMap;

public class EntityInstanceFactory {

	private HashMap<String, Entity> entities = new HashMap<String, Entity>();

	private void register(Entity entity)
	{
		entities.put(entity.getFactoryName(), entity);
	}
	
	public EntityInstanceFactory()
	{
		register(new BallEntity());
	}

	public Entity getEntity(String factoryName) {
		return entities.get(factoryName);
	}

	public EntityInstance create(String factoryName)
	{
		return getEntity(factoryName).createInstance();
	}
	
	public EntityInstance clone(EntityInstance oldInstance) {
		
		EntityInstance newInstance = create(oldInstance.entity.getFactoryName());
		newInstance.setFromInstance(oldInstance);
		return newInstance;
	}
	
	public void loadAssets()
	{
		for( Entity entity : entities.values() ) {
			
			entity.loadAssets();
		}
	}

	public void dispose()
	{
		for( Entity entity : entities.values() ) {
			
			entity.dispose();
		}
	}

} // class

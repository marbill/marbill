package org.henzenmann.marbill.common;


public class Tile {

	private int[] heights = new int[4];

	public enum Corner {
		TopLeft,
		TopRight, 
		BottomRight,
		BottomLeft
	}
	
	public enum Side {
		Top,
		Right,
		Bottom,
		Left
	}

	public static final int MinHeight = -10;
	public static final int MaxHeight =  10;
	
	public static final float HeightStep = 0.1f;
	
	public Tile()
	{
		heights[0] = 0;
		heights[1] = 0;
		heights[2] = 0;
		heights[3] = 0;
	}

	// Removed per-corner elevation for now...
	private void elevateCorner(final Corner corner, final int deltaHeight)
	{		
		final int i = corner.ordinal();
		final int h = heights[i];
		heights[i] = Math.max(MinHeight, Math.min(MaxHeight, h + deltaHeight));
	}
	
	public void elevate(final int deltaHeight)
	{
		for( int i = 0; i < 4; i++ ) {
			
			final int h = heights[i];
			heights[i] = Math.max(MinHeight, Math.min(MaxHeight, h + deltaHeight));
		}
	}
		
	public void elevateUp()
	{
		elevate(1);
	}
	
	public void elevateDown()
	{
		elevate(-1);
	}
	
	public void elevateSide(final Side side, final int deltaHeight)
	{
		switch(side) {
		
		case Top:
			elevateCorner(Corner.TopLeft, deltaHeight);
			elevateCorner(Corner.TopRight, deltaHeight);
			break;
			
		case Right:
			elevateCorner(Corner.TopRight, deltaHeight);
			elevateCorner(Corner.BottomRight, deltaHeight);
			break;
			
		case Bottom:
			elevateCorner(Corner.BottomRight, deltaHeight);
			elevateCorner(Corner.BottomLeft, deltaHeight);
			break;
			
		case Left:
			elevateCorner(Corner.BottomLeft, deltaHeight);
			elevateCorner(Corner.TopLeft, deltaHeight);
			break;
		}
	}
	
	public void elevateSideUp(final Side side)
	{
		elevateSide(side, 1);
	}
	
	public void elevateSideDown(final Side side)
	{
		elevateSide(side, -1);
	}
	
	public void zero()
	{
		for( int i = 0; i < 4; i++ ) {
			
			heights[i] = 0;
		}
	}

	public float getCornerHeight(final Corner corner) 
	{
		return HeightStep * heights[corner.ordinal()];
	}

	public int getCornerHeightI(final Corner corner) 
	{
		return heights[corner.ordinal()];
	}

	
	public int getHeight(int i) 
	{
		return heights[i];
	}
	
	public void setHeight(int i, int value)
	{
		heights[i] = value;
	}
}

package org.henzenmann.marbill.common;

import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.utils.Disposable;

public abstract class Entity implements Disposable {

	public abstract String getFactoryName();
	public abstract EntityInstance createInstance();
	
	public abstract void getRelBoundingBox(BoundingBox box);
	
	public abstract void renderInstance(EntityInstance instance, Camera camera);
	
	public abstract void loadAssets();
}

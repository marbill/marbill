package org.henzenmann.marbill.common;

import java.util.Vector;

import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.VertexAttribute;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.graphics.g3d.utils.MeshBuilder;
import com.badlogic.gdx.graphics.g3d.utils.MeshPartBuilder;
import com.badlogic.gdx.graphics.g3d.utils.MeshPartBuilder.VertexInfo;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;

public class Map {

	private Tile[] tiles;
	private int width;
	private int height;

	public Vector<EntityInstance> entityInstances = new Vector<EntityInstance>();
	
	public Map(final int width, final int height)
	{
		this.width = width;
		this.height = height;
		this.tiles = new Tile[width * height];	
		for( int i = 0; i < width * height; i++ ) {
			this.tiles[i] = new Tile();
		}
	}
	
	public void replaceEntities(EntityInstanceFactory entityInstanceFactory)
	{
		for(EntityInstance entityInstance : entityInstances ) {
			
			entityInstance.setEntity(entityInstanceFactory.getEntity(entityInstance.entity.getFactoryName()));
		}
	}
	
	public Mesh createIndexedMesh()
	{
		// Slow, builds indexed mesh usable by Bullet
		
		MeshBuilder meshBuilder = new MeshBuilder();
		
		meshBuilder.begin( new VertexAttributes( new VertexAttribute(Usage.Position, 3, ShaderProgram.POSITION_ATTRIBUTE),
												 new VertexAttribute(Usage.Normal, 3, ShaderProgram.NORMAL_ATTRIBUTE),
												 new VertexAttribute(Usage.Color, 4, ShaderProgram.COLOR_ATTRIBUTE) ),
						   GL10.GL_TRIANGLES );
				
		MeshPartBuilder.VertexInfo v0 = new MeshPartBuilder.VertexInfo();
		MeshPartBuilder.VertexInfo v1 = new MeshPartBuilder.VertexInfo();
		MeshPartBuilder.VertexInfo v2 = new MeshPartBuilder.VertexInfo();
		MeshPartBuilder.VertexInfo v3 = new MeshPartBuilder.VertexInfo();

		MeshPartBuilder.VertexInfo vB0 = new MeshPartBuilder.VertexInfo();
		MeshPartBuilder.VertexInfo vB1 = new MeshPartBuilder.VertexInfo();
		
		final float[] reds = {1.0f, 0.5f};
		final float[] greens = {1.0f, 0.5f};
		final float[] blues = {1.0f, 0.5f};

		Vector3 nAux = new Vector3();
		Vector3 n = new Vector3();
		
		int k = 0;
		for( int row = 0; row < height; row++ ) {
			for( int col = 0; col < width; col++ ) {
			
				Tile tile = tiles[k];

				final int color = (row + col) & 1; 
				
				final float r = reds[color];
				final float g = greens[color];
				final float b = blues[color];
				
				v0.setPos(    col,     tile.getCornerHeight(Tile.Corner.TopLeft),     row);
				v1.setPos(    col,  tile.getCornerHeight(Tile.Corner.BottomLeft), row + 1);
				v2.setPos(col + 1, tile.getCornerHeight(Tile.Corner.BottomRight), row + 1);
				v3.setPos(col + 1,    tile.getCornerHeight(Tile.Corner.TopRight),     row);

				buildQuad( meshBuilder, 
							v0, v1, v2, v3,
							r, g, b,
							nAux, n );
				
				final int colorB = (color + 1) & 1;
				final float rB = reds[colorB];
				final float gB = greens[colorB];
				final float bB = blues[colorB];
				
				if( col > 0 ) {
					
					Tile tileB = tiles[ k - 1 ];
		
					//vB0.reset();
					vB0.setPos(col, tileB.getCornerHeight(Tile.Corner.BottomRight), row + 1);
					
					//vB1.reset();
					vB1.setPos(col, tileB.getCornerHeight(Tile.Corner.TopRight), row);
										
					buildSide( meshBuilder,
								v1, v0, r, g, b,
								vB0, vB1, rB, gB, bB,
								nAux, n );
				}
				if( row > 0 ) {

					Tile tileB = tiles[ k - height ];
					
					//vB0.reset();
					vB0.setPos(col, tileB.getCornerHeight(Tile.Corner.BottomLeft), row);
					
					//vB1.reset();
					vB1.setPos(col + 1, tileB.getCornerHeight(Tile.Corner.BottomRight), row);
										
					buildSide( meshBuilder,
								v0, v3, r, g, b,
								vB0, vB1, rB, gB, bB,
								nAux, n );
				}
				
				k++;
			}
		}
		
		return meshBuilder.end();
	}
	
	private void buildTriangle( MeshBuilder meshBuilder,
								VertexInfo v0, VertexInfo v1, VertexInfo v2,
								float r, float g, float b,
								Vector3 nAux, Vector3 n )
	{
		nAux.set(v2.position);
		nAux.sub(v0.position);

		n.set(v1.position);
		n.sub(v0.position);

		n.crs(nAux);
		n.nor();

		v0.setNor(n);
		v0.setCol(r, g, b, 1f);

		v1.setNor(n);
		v1.setCol(r, g, b, 1f);

		v2.setNor(n);
		v2.setCol(r, g, b, 1f);

		meshBuilder.triangle(v0, v1, v2);
	}

	private void buildQuad( MeshBuilder meshBuilder,
							VertexInfo v0, VertexInfo v1, VertexInfo v2, VertexInfo v3,
							float r, float g, float b,
							Vector3 nAux, Vector3 n )
	{
		nAux.set(v2.position);
		nAux.sub(v0.position);

		n.set(v1.position);
		n.sub(v0.position);

		n.crs(nAux);
		n.nor();

		v0.setNor(n);
		v0.setCol(r, g, b, 1f);

		v1.setNor(n);
		v1.setCol(r, g, b, 1f);

		v2.setNor(n);
		v2.setCol(r, g, b, 1f);

		v3.setNor(n);
		v3.setCol(r, g, b, 1f);

		meshBuilder.triangle(v0, v1, v2);
		meshBuilder.triangle(v2, v3, v0);
	}
	
	private void buildSide( MeshBuilder meshBuilder, 
							VertexInfo vA0, VertexInfo vA1, float rA, float gA, float bA, 
							VertexInfo vB0, VertexInfo vB1, float rB, float gB, float bB,
							Vector3 nAux, Vector3 n )
	{
		if( Math.abs(vB0.position.y - vA0.position.y) < 0.01f ) {
			
			if( Math.abs( vB1.position.y - vA1.position.y ) < 0.01f ) {
				
				// Both corners are aligned, no triangles
				// need to be generated
			}
			else if( vB1.position.y < vA1.position.y ) {
				
				// Outward (clockwise)
				// b1, a0, a1
				buildTriangle( meshBuilder, 
								vB1, vA0, vA1,
								rA, gA, bA, 
								nAux, n );
			}
			else if( vB1.position.y > vA1.position.y ) {
				
				// Inward (counter-clockwise)
				// a0, a1, b1 
				buildTriangle( meshBuilder, 
								vA0, vA1, vB1,	
								rB, gB, bB, 
								nAux, n );
			}
		}
		else if( Math.abs( vB1.position.y - vA1.position.y ) < 0.01f ) {
			
			if( Math.abs( vB0.position.y - vA0.position.y ) < 0.01f ) {

				// NOP
			}
			else if( vB0.position.y < vA0.position.y ) {
				
				// a1, b0, a0
				buildTriangle( meshBuilder, 
								vA1, vB0, vA0,	
								rA, gA, bA, 
								nAux, n );
			}
			else if( vB0.position.y > vA0.position.y ) {
				
				// a0, a1, b1
				buildTriangle( meshBuilder, 
								vA0, vA1, vB0,	
								rB, gB, bB, 
								nAux, n );
			}
		}
		else if( vB0.position.y > vA0.position.y && vB1.position.y > vA1.position.y ) {
			
			// inward facing
			buildQuad( meshBuilder,
						vB0, vA0, vA1, vB1,
						rB, gB, bB, 
						nAux, n );
		}
		else if( vB0.position.y < vA0.position.y && vB1.position.y < vA1.position.y ) {
			
			// outward facing
			buildQuad( meshBuilder,
						vA1, vB1, vB0, vA0,
						rA, gA, bA, 
						nAux, n );
		}
		else if( vB0.position.y > vA0.position.y && vB1.position.y < vA1.position.y ) {
			
			buildTriangle(meshBuilder, vB0, vA0, vB1, rB, gB, bB, nAux, n);
			buildTriangle(meshBuilder, vB1, vA0, vB0, rA, gA, bA, nAux, n);
		}
		else if( vB0.position.y < vA0.position.y && vB1.position.y > vA1.position.y ) {	

			buildTriangle(meshBuilder, vB0, vA1, vB1, rB, gB, bB, nAux, n);
			buildTriangle(meshBuilder, vA1, vB0, vA0, rA, gA, bA, nAux, n);
		}
	}

	public Mesh createMesh()
	{
		// Very fast, but does not build sides, 
		// also does not build indexed mesh...
		
		final int VerticesPerTile = 6; // 2 triangles 
		final int FloatsPerVertex = 10; // position (3) + normal (3) + color (4)
		final int NumTiles = width * height;
		
		final float[] reds = {1.0f, 0.5f};
		final float[] greens = {1.0f, 0.5f};
		final float[] blues = {1.0f, 0.5f};
		
		int i = 0; // Tile index
		int j = 0; // Vertex index
		float vertices[] = new float[NumTiles * VerticesPerTile * FloatsPerVertex];
		
		int color = 0;
		
		for( int row = 0; row < height; row++ ) {
			for( int col = 0; col < width; col++) {
				Tile tile = tiles[i++];		

				final int k = color & 1; // Color index
				final float alpha = 1.0f;
				
				// First triangle
				
				vertices[j++] = col;
				vertices[j++] = tile.getCornerHeight(Tile.Corner.TopLeft);
				vertices[j++] = row;

				j += 3; // skip normals
				
				vertices[j++] = reds[k];
				vertices[j++] = greens[k];
				vertices[j++] = blues[k];
				vertices[j++] = alpha;

				vertices[j++] = col;
				vertices[j++] = tile.getCornerHeight(Tile.Corner.BottomLeft);
				vertices[j++] = row + 1;

				j += 3; // skip normals
				
				vertices[j++] = reds[k];
				vertices[j++] = greens[k];
				vertices[j++] = blues[k];
				vertices[j++] = alpha;

				vertices[j++] = col + 1;
				vertices[j++] = tile.getCornerHeight(Tile.Corner.BottomRight);
				vertices[j++] = row + 1;

				j += 3; // skip normals
		
				vertices[j++] = reds[k];
				vertices[j++] = greens[k];
				vertices[j++] = blues[k];
				vertices[j++] = alpha;

				// Second triangle
				
				vertices[j++] = col + 1;
				vertices[j++] = tile.getCornerHeight(Tile.Corner.BottomRight);
				vertices[j++] = row + 1;
				
				j += 3; // skip normals
				
				vertices[j++] = reds[k];
				vertices[j++] = greens[k];
				vertices[j++] = blues[k];
				vertices[j++] = alpha;
				
				vertices[j++] = col + 1;
				vertices[j++] = tile.getCornerHeight(Tile.Corner.TopRight);
				vertices[j++] = row;
				
				j += 3; // skip normals
				
				vertices[j++] = reds[k];
				vertices[j++] = greens[k];
				vertices[j++] = blues[k];
				vertices[j++] = alpha;

				vertices[j++] = col;
				vertices[j++] = tile.getCornerHeight(Tile.Corner.TopLeft);
				vertices[j++] = row;
				
				j += 3; // skip normals
				
				vertices[j++] = reds[k];
				vertices[j++] = greens[k];
				vertices[j++] = blues[k];
				vertices[j++] = alpha;

				color++;
			}
			
			color++;
		}
		
		Vector3 a = new Vector3();
		Vector3 b = new Vector3();
		Vector3 c = new Vector3();
		Vector3 e1 = new Vector3();
		Vector3 e2 = new Vector3();
		Vector3 n = new Vector3();
		
		j = 0; // Vertex index
		final int NumTris = 2 * width * height;
		for(i = 0; i < NumTris; i++) {
		
				a.set(vertices[                      j], vertices[                      j + 1], vertices[                      j + 2]); 
				b.set(vertices[    FloatsPerVertex + j], vertices[    FloatsPerVertex + j + 1], vertices[    FloatsPerVertex + j + 2]); 
				c.set(vertices[2 * FloatsPerVertex + j], vertices[2 * FloatsPerVertex + j + 1], vertices[2 * FloatsPerVertex + j + 2]);

				// a -> b
				e1.set(b);
				e1.sub(a);
				
				// a -> c
				e2.set(c);
				e2.sub(a);
				
				n.set(e1);
				n.crs(e2);
				n.nor();
				
				j += 3;
				vertices[ j++ ] = n.x;
				vertices[ j++ ] = n.y;
				vertices[ j++ ] = n.z;

				// b -> c
				e1.set(c);
				e1.sub(b);
				
				// b -> a
				e2.set(a);
				e2.sub(b);
				
				n.set(e1);
				n.crs(e2);
				n.nor();
				
				j += FloatsPerVertex - 3;
				vertices[ j++ ] = n.x;
				vertices[ j++ ] = n.y;
				vertices[ j++ ] = n.z;

				// c -> a
				
				e1.set(a);
				e1.sub(c);
				
				// c -> b
				e2.set(b);
				e2.sub(c);
				
				n.set(e1);
				n.crs(e2);
				n.nor();
				
				j += FloatsPerVertex - 3;
				vertices[ j++ ] = n.x;
				vertices[ j++ ] = n.y;
				vertices[ j++ ] = n.z;

				j += FloatsPerVertex - 6;
		}
		
		System.out.println("#vertices = " + NumTiles * VerticesPerTile);
		
		Mesh mesh = new Mesh(true, NumTiles * VerticesPerTile,  0,
							 new VertexAttribute(Usage.Position, 3, ShaderProgram.POSITION_ATTRIBUTE),
							 new VertexAttribute(Usage.Normal, 3, ShaderProgram.NORMAL_ATTRIBUTE),
							 new VertexAttribute(Usage.Color, 4, ShaderProgram.COLOR_ATTRIBUTE));
		mesh.setVertices(vertices);
		return mesh;
	}

	public Tile getTile(int x, int y) {
		
		if( x >= 0 && x < width && y >= 0 && y < height) {
			
			return tiles[x + y * height];
		}
		else {
			
			return null;
		}
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
	
	public void toJson(Json json)
	{
		json.writeObjectStart();
		json.writeValue("width", width);
		json.writeValue("height", height);
		json.writeValue("tiles", tiles);
		
		json.writeArrayStart("entities");
		for(EntityInstance entityInstance : entityInstances) {
			
			entityInstance.toJson(json);
		}
		json.writeArrayEnd();
		
		json.writeObjectEnd();
	}
	

	public static Map fromJson(Json json, JsonValue jsonValue, EntityInstanceFactory entityInstanceFactory) {
		
		Map map = new Map(jsonValue.getInt("width"), jsonValue.getInt("height"));
		map.tiles = json.readValue(Tile[].class, jsonValue.get("tiles"));
		
		JsonValue entities = jsonValue.get("entities");
		if( entities != null ) {
			for( JsonValue child = entities.child(); child != null; child = child.next() ) {
		
				EntityInstance entityInstance = entityInstanceFactory.create(child.get("factoryName").asString());
				entityInstance.setFromJson(json, child);	
				map.entityInstances.add(entityInstance);
			}
		}
		
		return map;
	}

	public Map clone(EntityInstanceFactory entityInstanceFactory) 
	{
		Map map = new Map(width, height);
		map.tiles = tiles.clone();
		
		for( EntityInstance oldInstance : entityInstances ) {
			
			map.entityInstances.add(entityInstanceFactory.clone(oldInstance));
		}

		return map;
	}

	public EntityInstance getFirstEntityInstance(String string) 
	{
		for( EntityInstance instance : entityInstances ) {
			
			if(instance.entity.getFactoryName().equalsIgnoreCase(string)) {
				
				return instance;
			}
		}

		return null;
	}
}

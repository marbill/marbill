package org.henzenmann.marbill.common;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;

public class BallEntity extends Entity {

	private Texture texture;
	private Mesh mesh;
	private Vector3 normal = new Vector3(0f, 0f, 1f); 
	private Vector3 up = new Vector3(0f, 1f, 0f);
	
	@Override
	public String getFactoryName() {
		return "ball";
	}

	@Override
	public void getRelBoundingBox(BoundingBox box) {
		box.min.set(-0.5f, -0.5f, -0.5f);
		box.max.set( 0.5f,  0.5f,  0.5f);		
	}
	
	@Override
	public void loadAssets()
	{
		texture = new Texture(Gdx.files.internal("textures/ball256.png"));
		mesh = MeshUtils.createSprite(1f, 1f);
	}
	
	@Override
	public void dispose() {
		
		mesh.dispose();
		texture.dispose();		
	}
	
	@Override
	public void renderInstance(EntityInstance instance, Camera camera) {
		
		camera.applyBillboardRotation(Gdx.gl10, instance.position, normal, up);	
	
		Gdx.gl10.glEnable(GL10.GL_TEXTURE_2D);
		
		texture.bind();
		mesh.render(GL10.GL_TRIANGLES);

		Gdx.gl10.glDisable(GL10.GL_TEXTURE_2D);
	}

	@Override
	public EntityInstance createInstance() {
		return new BallInstance(this);
	}
}

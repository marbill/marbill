package org.henzenmann.marbill.common;

import org.henzenmann.marbill.physics.Body;

import com.badlogic.gdx.math.Vector3;



public class BallInstance extends EntityInstance {

	public Body body;
	
	BallInstance(Entity entity) {
		super(entity);

		body = new Body(this);
		
		position.set(32f, 2f, 32f);
		//body.worldTransform.setTranslation(position);
		//body.rigidBody.setWorldTransform(body.worldTransform);
	}
	
	public void setupPhysics()
	{
		body.worldTransform.setTranslation(position);
		body.rigidBody.setWorldTransform(body.worldTransform);
	}
	
	private Vector3 impulse = new Vector3();
	
	public void update()
	{
		body.rigidBody.activate(true);
		body.rigidBody.applyCentralImpulse(impulse);
		impulse.set(0f, 0f, 0f);
	}
	
	public void applyImpulse(Vector3 impulse)
	{
		this.impulse.add(impulse);
	}
	
}

package org.henzenmann.marbill.common;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.BoundingBox;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonValue;

public abstract class EntityInstance {

	public Entity entity; 
	public Vector3 position = new Vector3();
	
	EntityInstance(Entity entity)
	{
		this.entity = entity;
	}
	
	void setEntity(Entity entity)
	{
		this.entity = entity;
	}

	public void getAbsBoundingBox(BoundingBox box)
	{
		entity.getRelBoundingBox(box);
		box.min.add(position);
		box.max.add(position);
	}
	
	public void render(Camera camera) {

		Gdx.gl10.glPushMatrix();
		Gdx.gl10.glTranslatef(position.x,  position.y, position.z);
		
		entity.renderInstance(this, camera);
		
		Gdx.gl10.glPopMatrix();
	}
	
	public void toJson(Json json)
	{
		json.writeObjectStart();
		json.writeValue("factoryName", entity.getFactoryName());
		json.writeValue("position", position);
		json.writeObjectEnd();
	}
	
	public void setFromJson(Json json, JsonValue jsonValue)
	{
		position = json.readValue(Vector3.class, jsonValue.get("position"));
	}

	public void setFromInstance(EntityInstance oldInstance) 
	{
		position.set(oldInstance.position);
	}
}

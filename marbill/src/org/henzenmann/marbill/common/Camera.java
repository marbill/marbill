package org.henzenmann.marbill.common;

import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector3;

public class Camera {

	public PerspectiveCamera camera;

	public Camera(PerspectiveCamera camera)
	{
		this.camera = camera;
	}

	public float distance = 16f;
	public float angle = 45f;
	public float rotation = 270f;
	public Vector3 position = new Vector3(0, 0, 0);

	private Vector3 _objToCamera = new Vector3();
	private Vector3 _up = new Vector3();
	private Quaternion _q1 = new Quaternion();
	private Quaternion _q2 = new Quaternion();
	private float[] _m = new float[16];

	public void applyBillboardRotation(GL10 gl, Vector3 objPosition, Vector3 objNormal, Vector3 objUp)
	{
		_objToCamera.set(camera.position);
		_objToCamera.sub(objPosition);
		_objToCamera.nor();
		
		_q1.setFromCross(objNormal, _objToCamera);
		
		_up.set(objUp);
		_up.mul(_q1);
		
		_q2.setFromCross(_up, camera.up);
		_q2.mul(_q1);

		_q2.toMatrix(_m);

		gl.glMultMatrixf(_m,  0);
	}

	/**
	 * Updates the camera matrix
	 */
	public void update() {
		Vector3 invDir = new Vector3(1, 0, 0).rotate(angle, 0,  0, 1).rotate(rotation,  0,  1,  0);
		
		camera.position.set(position.cpy().add(invDir.cpy().scl(distance)));
		camera.up.set(0, 1, 0);
		camera.direction.set(invDir.cpy().scl(-1));
		camera.normalizeUp();
		camera.update();
	}
	
}
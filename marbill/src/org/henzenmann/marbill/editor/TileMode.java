package org.henzenmann.marbill.editor;

import java.util.HashMap;

import org.henzenmann.marbill.common.Tile;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;

public class TileMode extends EditorMode {

	/**
	 * Abstract keyboard commands understood by the editor
	 */
	private enum Keys {
		
		KeyElevateTile,
		KeyElevateTop,
		KeyElevateLeft,
		KeyElevateBottom,
		KeyElevateRight,
//		KeyElevateTopLeft,
//		KeyElevateBottomLeft,
//		KeyElevateBottomRight,
//		KeyElevateTopRight,
		KeyElevateZero
	}
	
	// Command implementations
	
	private abstract class ElevateCommand implements Command
	{
		protected EditorScreen editor;
		protected int i;
		protected int j;
		protected int deltaHeight;
	
		ElevateCommand(EditorScreen editor, int i, int j, int deltaHeight)
		{
			this.editor = editor;
			this.i = i;
			this.j = j;
			this.deltaHeight = deltaHeight;
		}
		
		protected Tile getTile()
		{
			return editor.getMap().getTile(i, j);
		}
	}
	
	private class ElevateTileCommand extends ElevateCommand
	{
		
		public ElevateTileCommand(EditorScreen editor, int i, int j, int deltaHeight)
		{
			super(editor, i, j, deltaHeight);
		}
		
		@Override
		public void exec()
		{
			getTile().elevate(deltaHeight);
			editor.setMapDirty();
		}
		
		@Override
		public void undo()
		{
			getTile().elevate(-deltaHeight);
			editor.setMapDirty();
		}
	}
	
//	private class ElevateCornerCommand extends ElevateCommand
//	{
//		private Corner corner;
//
//		public ElevateCornerCommand(EditorScreen editor, Tile.Corner corner, int i, int j, int deltaHeight)
//		{
//			super(editor, i, j, deltaHeight);
//			this.corner = corner;
//		}
//		
//		@Override
//		public void exec()
//		{
//			getTile().elevateCorner(corner, deltaHeight);
//			editor.setMapDirty();
//		}
//		
//		@Override
//		public void undo()
//		{
//			getTile().elevateCorner(corner, -deltaHeight);
//			editor.setMapDirty();
//		}
//	}
	
	private class ElevateSideCommand extends ElevateCommand
	{
		private Tile.Side side;

		public ElevateSideCommand(EditorScreen editor, Tile.Side side, int i, int j, int deltaHeight)
		{
			super(editor, i, j, deltaHeight);
			this.side = side;
		}
		
		@Override
		public void exec()
		{
			getTile().elevateSide(side, deltaHeight);
			editor.setMapDirty();
		}
		
		@Override
		public void undo()
		{
			getTile().elevateSide(side, -deltaHeight);
			editor.setMapDirty();
		}
	}

	private class ElevateZeroCommand extends ElevateCommand 
	{
		private int[] heights = new int[4];
		
		public ElevateZeroCommand(EditorScreen editor, int i, int j)
		{
			super(editor, i, j, 0);
		}
		
		@Override
		public void exec()
		{
			Tile tile = getTile();
			for( int i = 0; i < 4; i++ ) {
			
				this.heights[i] = tile.getHeight(i);
			}
			
			tile.zero();
			editor.setMapDirty();
		}
		
		@Override
		public void undo()
		{
			Tile tile = getTile();
			for( int i = 0; i < 4; i++ ) {
				
				tile.setHeight(i, heights[i]);
			}
			
			editor.setMapDirty();
		}
	}

	private HashMap<Keys, Integer> keyMap = new HashMap<Keys, Integer>();

	TileMode(EditorScreen editor)
	{
		super(editor);
		
		keyMap.put(Keys.KeyElevateTile, Input.Keys.L);
//		keyMap.put(Keys.KeyElevateTopLeft, Input.Keys.H);
//		keyMap.put(Keys.KeyElevateBottomLeft, Input.Keys.G);
//		keyMap.put(Keys.KeyElevateBottomRight, Input.Keys.F);
//		keyMap.put(Keys.KeyElevateTopRight, Input.Keys.D);
		keyMap.put(Keys.KeyElevateTop, Input.Keys.G);
		keyMap.put(Keys.KeyElevateLeft, Input.Keys.F);
		keyMap.put(Keys.KeyElevateBottom, Input.Keys.H);
		keyMap.put(Keys.KeyElevateRight, Input.Keys.J);
		keyMap.put(Keys.KeyElevateZero, Input.Keys.NUM_0);
	}
	
	private void elevateTile(boolean up)
	{
		if( editor.getCursor().enabled ) {
			
			int i = (int) editor.getCursor().position.x;
			int j = (int) editor.getCursor().position.z;
			
			if( editor.getMap().getTile(i, j) != null ) {
		
				editor.commandStack.exec(new ElevateTileCommand( editor, i, j, up ? 1 : -1) );
			}
		}
	}

//	private void elevateCorner(Tile.Corner corner, boolean up)
//	{
//		if( cursor.enabled ) {
//			
//			int i = (int) cursor.position.x;
//			int j = (int) cursor.position.z;
//			
//			if( map.getTile(i, j) != null ) {
//				
//				commandStack.exec(new ElevateCornerCommand(this, corner, i, j, up ? 1 : -1));
//			}
//		}
//	}

	private void elevateSide(Tile.Side side, boolean up)
	{
		if( editor.getCursor().enabled ) {
			
			int i = (int) editor.getCursor().position.x;
			int j = (int) editor.getCursor().position.z;
			
			if( editor.getMap().getTile(i, j) != null ) {
				
				editor.commandStack.exec(new ElevateSideCommand(editor, side, i, j, up ? 1 : -1));
			}
		}
	}

	private void elevateZero()
	{
		if( editor.getCursor().enabled ) {
			
			int i = (int) editor.getCursor().position.x;
			int j = (int) editor.getCursor().position.z;
			
			if( editor.getMap().getTile(i, j) != null ) {
				
				editor.commandStack.exec(new ElevateZeroCommand(editor, i, j));
			}
		}
	}

	@Override
	boolean keyDown(int keycode)
	{
		final boolean shift = Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT) ||
				Gdx.input.isKeyPressed(Input.Keys.SHIFT_RIGHT);

		if( keycode == keyMap.get(Keys.KeyElevateTile) ) {

			elevateTile(!shift);
		}
		else if( keycode == keyMap.get(Keys.KeyElevateTop) ) {

			elevateSide(Tile.Side.Top, !shift);
		}
		else if( keycode == keyMap.get(Keys.KeyElevateLeft) ) {

			elevateSide(Tile.Side.Left, !shift);
		}
		else if( keycode == keyMap.get(Keys.KeyElevateBottom) ) {

			elevateSide(Tile.Side.Bottom, !shift);
		}
		else if( keycode == keyMap.get(Keys.KeyElevateRight) ) {

			elevateSide(Tile.Side.Right, !shift);
		}
		else if( keycode == keyMap.get(Keys.KeyElevateZero) ) {

			elevateZero();
		}

		return false;
	}
	
} // class

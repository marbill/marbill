package org.henzenmann.marbill.editor;

import org.henzenmann.marbill.common.Camera;

import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector3;

/**
 * Wrapper around the editor's camera and its state.
 */
public class EditorCamera extends Camera
{
	private static final float MinTiltingAngle =  1f;
	private static final float MaxTiltingAngle = 89f;
	
	private static final float MinDistance = 1f;
	private static final float MaxDistance = 20f;
	
	/**
	 * @param w Viewport width
	 * @param h Viewport height
	 */
	public EditorCamera(float w, float h, float x, float y)
	{
		super(new PerspectiveCamera(60, w, h));
		camera.near = 0.1f;
		camera.far = 100f;
		
		position.set(x, 0, y);
		update();
	}
	
	/**
	 * Rotate camera around y-axis
	 * 
	 * Updates the camera matrix.
	 * 
	 * @param deltaAngle Relative rotation in degrees
	 */
	public void rotate(float deltaAngle)
	{
		rotateNoUpdate(deltaAngle);
		update();
	}

	/**
	 * Rotate around y-axis without updating the camera matrix
	 * 
	 * @param deltaAngle Relative rotation in degrees
	 */
	public void rotateNoUpdate(float deltaAngle)
	{
		rotation += deltaAngle;	
	}

	
	/**
	 * Tilt camera angle 
	 * 
	 * Updates the camera matrix.
	 * 
	 * @param deltaAngle The relative angle in degrees
	 */
	public void tilt(float deltaAngle)
	{
		tiltNoUpdate(deltaAngle);
		update();
	}
	
	/**
	 * Tilt camera angle without updating the camera matrix
	 * 
	 * @param deltaAngle The relative angle in degress
	 */
	public void tiltNoUpdate(float deltaAngle)
	{
		angle = MathUtils.clamp(angle + deltaAngle, MinTiltingAngle, MaxTiltingAngle);
	}

	/**
	 * Moves the look-at position.
	 * 
	 * The coordinates are given in a virtual plane and
	 * are interpreted as forward/sideways in relation to 
	 * the current camera orientation.
	 * 
	 * @param dx Relative x-coordinate
	 * @param dy Relative y-coordinate (actually mapped to z-plane)
	 */
	public void moveBy(float dx, float dy) {
		position.sub(new Vector3(dx, 0, dy).rotate(rotation - 270f, 0, 1, 0));
		update();
	}

	public void zoomBy(float value) {
		distance = MathUtils.clamp(distance + value, MinDistance, MaxDistance);
		update();
	}
	

//	private String lastDump = new String();
//	
//	void dump(String s)
//	{
//		if( !s.equals(lastDump) ) {
//			
//			lastDump = s;
//			System.out.println(s);
//		}
//	}
//	
//	private static String vfmt(Vector3 vector)
//	{
//		return String.format("(% .2f % .2f % .2f)", vector.x, vector.y, vector.z);
//	}
//	
//	private static String afmt(float a)
//	{
//		return String.format("% 7.2f", a);
//	}


}
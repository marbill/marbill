package org.henzenmann.marbill.editor;

class CameraDrag
{
	private enum Mode {
		Rotate,
		Pan
	}
	
	private EditorCamera camera;
	private Mode mode = Mode.Rotate;
	boolean enabled = false;
	private int startX;
	private int startY;
	
	public CameraDrag(EditorCamera camera)
	{
		this.camera = camera;
	}
	
	private void start(Mode mode, int x, int y)
	{
		this.mode = mode;
		enabled = true;
		startX = x;
		startY = y;
	}

	public void startRotation(int screenX, int screenY) {
		
		start(Mode.Rotate, screenX, screenY);
	}

	public void startPanning(int screenX, int screenY) {
		
		start(Mode.Pan, screenX, screenY);
	}

	public void update(int x, int y)
	{
		final float dx = x - startX;
		final float dy = startY - y; // y-axis is inverted for screen coordinates 

		startX = x;
		startY = y;
		
		switch(mode) {
		
		case Rotate:
			// TODO: Scale needs to depend on display resolution
			camera.rotateNoUpdate(dx * 0.4f);
			camera.tiltNoUpdate(dy * 0.4f);
			camera.update();
			break;
			
		case Pan:
			// TODO: Scale needs to depend on display resolution
			camera.moveBy(dx * 0.02f, dy * -0.02f);
			break;
		}
	}	
	
	public void stop(int x, int y)
	{
		update(x, y);
		enabled = false;
	}
}
package org.henzenmann.marbill.editor;

public abstract class EditorMode {

	protected EditorScreen editor;
	
	EditorMode(EditorScreen editor)
	{
		this.editor = editor;
	}

	abstract boolean keyDown(int keycode);
}

package org.henzenmann.marbill.editor;

import java.util.Stack;

class CommandStack
{
	private Stack<Command> undoStack = new Stack<Command>();
	private Stack<Command> redoStack = new Stack<Command>();
	
	void exec(Command command)
	{
		redoStack.clear();
		command.exec();
		undoStack.push(command);
	}
	
	void undo()
	{
		if( !undoStack.empty() ) {
			
			Command command = undoStack.pop();
			command.undo();
			redoStack.push(command);
		}
	}
	
	void redo()
	{
		if( !redoStack.empty() ) {
			
			Command command = redoStack.pop();
			command.exec();
			undoStack.push(command);
		}
	}

	public void clear() {
		
		redoStack.clear();
		undoStack.clear();
	}
}
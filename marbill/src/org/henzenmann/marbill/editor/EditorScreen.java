package org.henzenmann.marbill.editor;

import java.io.IOException;
import java.io.Writer;

import org.henzenmann.marbill.Marbill;
import org.henzenmann.marbill.common.EntityInstance;
import org.henzenmann.marbill.common.EntityInstanceFactory;
import org.henzenmann.marbill.common.Map;
import org.henzenmann.marbill.game.GameScreen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GL11;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;

public class EditorScreen implements Screen, InputProcessor {
	
	// Members
	
	private Marbill game;
	
	private Map map = new Map(64, 64);
	private Mesh mapMesh;
	private boolean mapDirty = true;

	private EditorCamera camera;
	private CameraDrag cameraDrag;
	
	private Cursor cursor;
	
	CommandStack commandStack = new CommandStack();

	private EditorMode mode;

	private EntityInstanceFactory entityInstanceFactory = new EntityInstanceFactory();

	// Methods
	
	public EditorScreen(Marbill game)
	{
		this.game = game;
		
		mode = new TileMode(this);
	}
	
	public Map getMap()
	{
		return map;
	}

	private void refreshMapMesh()
	{
		if( mapMesh != null ) {
			
			mapMesh.dispose();
		}
		
		//mapMesh = map.createIndexedMesh(true);
		mapMesh = map.createIndexedMesh();
		mapDirty = false;
	}

	public void setMapDirty()
	{
		mapDirty = true;
	}

	public Cursor getCursor()
	{
		return cursor;
	}
	
//	private Tile getTileUnderCursor()
//	{
//		if( cursor.enabled ) {
//			
//			return map.getTile((int) cursor.position.x, (int) cursor.position.z);
//		}
//		else {
//			
//			return null;
//		}
//	}


	
	// Screen interface
	
	@Override
	public void render(float delta) {      
		
		if( mapDirty ) {
			
			refreshMapMesh();
		}
		
		//Gdx.gl10.glClearColor(1,  0,  0,  1);
		Gdx.gl10.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
		Gdx.gl10.glMatrixMode(GL11.GL_MODELVIEW_MATRIX);
		Gdx.gl10.glLoadIdentity();
		
		camera.camera.apply(Gdx.gl10);

		// Lights

		float[] vAmbient = { 0.1f, 0.1f, 0.1f, 1f };
		Gdx.gl10.glLightfv(GL10.GL_LIGHT0, GL10.GL_AMBIENT, vAmbient, 0);

		float[] vDiffuse = { 0.8f, 0.8f, 0.8f, 1f };
		Gdx.gl10.glLightfv(GL10.GL_LIGHT0, GL10.GL_DIFFUSE, vDiffuse, 0);

		float[] vSpecular = { 0f, 0f, 0f, 1f };
		Gdx.gl10.glLightfv(GL10.GL_LIGHT0, GL10.GL_SPECULAR, vSpecular, 0);

		float[] vPosition = { 0f, 20f, 0f, 0f };
		Gdx.gl10.glLightfv(GL10.GL_LIGHT0, GL10.GL_POSITION, vPosition, 0);

		float[] vDirection = { 1, 0, -1, 0 };
		Gdx.gl10.glLightfv(GL10.GL_LIGHT0, GL10.GL_SPOT_DIRECTION, vDirection, 0);

		Gdx.gl10.glEnable(GL10.GL_LIGHT0);
		//Gdx.gl10.glShadeModel(GL10.GL_FLAT);
		Gdx.gl10.glShadeModel(GL10.GL_SMOOTH);

		// Render map mesh
		
		Gdx.gl10.glEnable(GL10.GL_DEPTH_TEST);
		Gdx.gl10.glEnable(GL10.GL_COLOR_MATERIAL);
		Gdx.gl10.glEnable(GL10.GL_LIGHTING);

		Gdx.gl10.glColor4f(1f,  1f,  1f,  1f);
		mapMesh.render( GL10.GL_TRIANGLES );

		Gdx.gl10.glDisable(GL10.GL_LIGHTING);
		Gdx.gl10.glDisable(GL10.GL_COLOR_MATERIAL);
		Gdx.gl10.glDisable(GL10.GL_DEPTH_TEST);
		

		Gdx.gl10.glShadeModel(GL10.GL_FLAT);

		// Render entities
		
		for( EntityInstance entityInstance : map.entityInstances ) {
			
			entityInstance.render(camera);
		}
		
		// Render cursor
		
		if( cursor.enabled ) {

			cursor.render();	
		}
	}


	@Override
	public void resize(int width, int height) {

		Gdx.gl10.glViewport(0, 0, width, height);
		
		final float ratio = (float) width / (float) height;
		camera = new EditorCamera(2f * ratio, 2f, 0.5f * map.getWidth(), 0.5f * map.getHeight());
		camera.update();
		cameraDrag = new CameraDrag(camera);
		
		cursor.enabled = true;
		// TODO: Make this lazy?
		cursor.updatePosition(camera.camera, width / 2, height / 2);
	}

	@Override
	public void show() {
		
		Gdx.input.setInputProcessor(this);
		
		refreshMapMesh();
		
		cursor = new Cursor();
		entityInstanceFactory.loadAssets();
		
		if( map.entityInstances.isEmpty() ) {
			
			// HACK
			
			EntityInstance ball = entityInstanceFactory.create("ball");
			ball.position.set(0.5f + 0.5f * map.getWidth(), 0.5f, 0.5f + 0.5f * map.getWidth());
			map.entityInstances.add(ball);
		}
	}

	@Override
	public void hide() {
		
		mapMesh.dispose();
		mapDirty = true;
		
		cursor.dispose();
		entityInstanceFactory.dispose();
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub
	}

	// InputProcessor interface
	
	@Override
	public boolean keyDown(int keycode) {
		
		boolean shift = Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT) ||
						Gdx.input.isKeyPressed(Input.Keys.SHIFT_RIGHT);
		
		boolean ctrl = Gdx.input.isKeyPressed(Input.Keys.CONTROL_LEFT) ||
					   Gdx.input.isKeyPressed(Input.Keys.CONTROL_RIGHT);
		
		
		if( ctrl && shift && keycode == Input.Keys.Z ) {
			
			commandStack.redo();
		}
		else if( ctrl && keycode == Input.Keys.Z ) {
		
			commandStack.undo();
		}
		else if( ctrl && keycode == Input.Keys.S ) {
			
			FileHandle fileHandle = Gdx.files.local("level.json");
			Json json = new Json();
			Writer writer = fileHandle.writer(false);
			json.setWriter(writer);
			map.toJson(json);
			try {
				writer.flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else if( ctrl && keycode == Input.Keys.O ) {
			
			FileHandle fileHandle = Gdx.files.local("level.json");
			JsonReader jsonReader = new JsonReader();
			JsonValue jsonValue = jsonReader.parse(fileHandle);
			Json json = new Json();
			Map map = Map.fromJson(json, jsonValue, entityInstanceFactory);
			
			this.commandStack.clear();
			this.cursor.enabled = false;
			this.map = map;
			this.setMapDirty();
		}
		else if( keycode == Input.Keys.F5 ) {
			
			game.setScreen(new GameScreen(game, map));
		}
		else { 
		
			// Delegate to current mode
			return mode.keyDown(keycode);
		}
		
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		
		// TODO: Cursor-catching
		
		if( button == Buttons.RIGHT) {

			if( !cameraDrag.enabled ) {
				
				cameraDrag.startRotation(screenX, screenY);
				cursor.enabled = false;
			}
		}
		else if( button == Buttons.MIDDLE ) {
			
			if( !cameraDrag.enabled ) {
				
				cameraDrag.startPanning(screenX, screenY);
				cursor.enabled = false;
			}
		}
		
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {

		if( cameraDrag.enabled ) {
				
			cameraDrag.stop(screenX, screenY);
			cursor.enabled = true;
			// TODO: Make this lazy?
			cursor.updatePosition(camera.camera, screenX, screenY);
		}
		
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {

		if( cameraDrag.enabled ) {
			
			cameraDrag.update(screenX, screenY);
		}
					
		return false;
	}
	
	@Override
	public boolean mouseMoved(int screenX, int screenY) {
	
		if( cursor.enabled ) {

			// TODO: Make this lazy?
			cursor.updatePosition(camera.camera, screenX, screenY);
		}
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		
		camera.zoomBy(0.4f * amount);
		return false;
	}

} // class

package org.henzenmann.marbill.editor;

interface Command
{
	void exec();
	void undo();
}
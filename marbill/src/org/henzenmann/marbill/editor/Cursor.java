package org.henzenmann.marbill.editor;

import org.henzenmann.marbill.common.MeshUtils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Plane;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.Ray;
import com.badlogic.gdx.utils.Disposable;

public class Cursor implements Disposable {

	public boolean enabled = false;
	public Vector3 position = new Vector3(0, 0, 0);
	public Mesh mesh;

	public Cursor()
	{
		mesh = MeshUtils.createBox(1, 1 ,1, 1, 0, 0, 0.3f);
	}
	
	public void setPosition(float x, float z)
	{
		position.set(x, 0, z);
	}
	
	void render()
	{
		Gdx.gl10.glPushMatrix();
		Gdx.gl10.glTranslatef(position.x + 0.5f, position.y + 0.5f, position.z + 0.5f);
		Gdx.gl10.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		Gdx.gl10.glEnable(GL10.GL_BLEND);
		Gdx.gl10.glDisable(GL10.GL_DEPTH_TEST);
		mesh.render(GL10.GL_TRIANGLES);
		Gdx.gl10.glPopMatrix();
	}

	private Plane plane = new Plane(new Vector3(0, 1, 0), 0);
	private Vector3 intersection = new Vector3();
	
	void updatePosition(PerspectiveCamera camera, int screenX, int screenY)
	{
		Ray ray = camera.getPickRay(screenX, screenY); // TODO: Avoid allocation?
		if( Intersector.intersectRayPlane(ray, plane, intersection)) {

			position.set( MathUtils.floor(intersection.x),
					      0f,
						  MathUtils.floor(intersection.z) );
		}
	}
	
	@Override
	public void dispose() {
		mesh.dispose();
	}
}

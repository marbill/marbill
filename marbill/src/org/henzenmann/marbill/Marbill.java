
package org.henzenmann.marbill;

import org.henzenmann.marbill.editor.EditorScreen;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.physics.bullet.Bullet;

public class Marbill extends Game {
	
	@Override
	public void create() {
		
		Bullet.init();
		
		setScreen(new EditorScreen(this));
	}
}

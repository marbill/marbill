package org.henzenmann.marbill.game;

import org.henzenmann.marbill.common.MeshUtils;

import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Plane;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.Ray;

public class BallDrag {

	boolean enabled = false;
	int endX;
	int endY;
	
	private Mesh arrowMeshHead;
	private Mesh arrowMeshTrunk;
	
	void start(int screenX, int screenY)
	{
		enabled = true;
		endX = screenX;
		endY = screenY;
	}
	
	void update(int screenX, int screenY)
	{
		endX = screenX;
		endY = screenY;
	}
	
	public void stop(int screenX, int screenY) {
		
		update(screenX, screenY);
		enabled = false;
	}
	
	Vector3 getArrowDir()
	{
		return arrowDir;
	}
	
	
	private Vector3 arrowPos = new Vector3();
	private Vector3 arrowDir = new Vector3();
	private float arrowAngle;
	private float arrowLen;

	private Plane plane = new Plane(Vector3.Y, 0);
	private Vector3 intersection = new Vector3();

	public void update(PerspectiveCamera camera, Vector3 ballPosition)
	{	
		// Determine cursor position in plane of the ball
		
		plane.set(ballPosition, Vector3.Y);
		Ray ray = camera.getPickRay(endX, endY);
		if( Intersector.intersectRayPlane(ray, plane, intersection) ) {

			// Determine distance of cursor from ball
			// Limit distance
		
			intersection.sub(ballPosition); // intersection now points into opposite shot direction
			
			final float MaxDistanceFromBallPosition = 3f;
			
			float distanceFromBallPosition = intersection.len();
			if( distanceFromBallPosition > MaxDistanceFromBallPosition ) {
				
				intersection.nor();
				intersection.scl(MaxDistanceFromBallPosition);
				distanceFromBallPosition = MaxDistanceFromBallPosition;
			}

			// Calculate shot dir
			
			arrowDir.set(intersection);
			arrowDir.scl(-1);
			arrowDir.nor();

			arrowPos.set(ballPosition);
			arrowPos.add(intersection);
			
			// Determine length of arrow
			arrowLen = 3f * distanceFromBallPosition;
						
			arrowAngle = MathUtils.radDeg * (MathUtils.atan2(-arrowDir.z, arrowDir.x) - MathUtils.PI * 0.5f);
		}
	}

	public void render(GL10 gl)
	{	
		// Translate to ball pos
		gl.glPushMatrix();
		gl.glTranslatef(arrowPos.x, arrowPos.y, arrowPos.z);

		// Rotate to shot dir around y-axis at ball pos
		gl.glRotatef(arrowAngle,  0f,  1f,  0f);

		// Translate to middle of arrow
		gl.glTranslatef(0f, 0f, -(arrowLen * 0.5f));

		// Scale to arrow lenght
		gl.glPushMatrix();
		gl.glScalef(1f, 1f, arrowLen);

		// Draw arrow
		gl.glDisable(gl.GL_DEPTH_TEST);
		gl.glColor4f(1f,  0f,  0f,  1f);

		arrowMeshTrunk.render(gl.GL_TRIANGLES);

		// Restore matrix from before scaling
		gl.glPopMatrix();

		// Translate to end of arrow
		gl.glTranslatef(0f,  0f, -(arrowLen * 0.5f));

		// Render arrow
		arrowMeshHead.render(gl.GL_TRIANGLES);

		gl.glColor4f(1f,  1f,  1f,  1f);
		gl.glEnable(gl.GL_DEPTH_TEST);

		// Restore matrix
		gl.glPopMatrix();
	}
	
	
	void loadAssets()
	{
		arrowMeshHead = MeshUtils.createArrowHead();
		arrowMeshTrunk = MeshUtils.createArrowTrunk();
	}
	
	void dispose()
	{
		arrowMeshHead.dispose();
		arrowMeshTrunk.dispose();
	}

	public float getArrowLen() {
		return arrowLen;
	}
}

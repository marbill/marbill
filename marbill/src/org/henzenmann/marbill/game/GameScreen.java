package org.henzenmann.marbill.game;

import org.henzenmann.marbill.Marbill;
import org.henzenmann.marbill.common.BallInstance;
import org.henzenmann.marbill.common.EntityInstance;
import org.henzenmann.marbill.common.EntityInstanceFactory;
import org.henzenmann.marbill.common.Map;
import org.henzenmann.marbill.physics.Physics;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.GL11;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.bullet.btBvhTriangleMeshShape;
import com.badlogic.gdx.physics.bullet.btDefaultMotionState;
import com.badlogic.gdx.physics.bullet.btRigidBody;
import com.badlogic.gdx.physics.bullet.btRigidBodyConstructionInfo;

public class GameScreen implements Screen, InputProcessor {

	private Marbill marbill;
	private Map map;
	private Mesh mapMesh;
	private btBvhTriangleMeshShape mapBodyShape;
	
	private EntityInstanceFactory entityInstanceFactory = new EntityInstanceFactory();
	private GameCamera camera;
	private BallInstance ball;

	private BallDrag ballDrag = new BallDrag();

	private Physics physics = new Physics();

	public GameScreen(Marbill marbill, Map map)
	{
		this.marbill = marbill;
		
		this.map = map.clone(entityInstanceFactory);
		this.ball = (BallInstance) this.map.getFirstEntityInstance("ball");
	}
	
	@Override
	public void render(float delta) {

		//ball.update();
		physics.dynamicsWorld.stepSimulation(delta);
		
		ball.body.updatePhysics();
		ball.update();
		
		Gdx.gl10.glClearColor(1,  0,  0,  1);
		Gdx.gl10.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
        Gdx.gl10.glMatrixMode(GL11.GL_MODELVIEW_MATRIX);
		Gdx.gl10.glLoadIdentity();
		
		Gdx.gl10.glShadeModel(GL10.GL_FLAT);
		Gdx.gl10.glDisable(GL10.GL_LIGHTING);
		Gdx.gl10.glDisable(GL10.GL_TEXTURE_2D);
		
		camera.setBallPosition(ball.position);
		camera.camera.apply(Gdx.gl10);

		mapMesh.render(GL10.GL_TRIANGLES);
		
		for( EntityInstance entityInstance : map.entityInstances ) {
			
			entityInstance.render(camera);
		}
		
		if( ballDrag.enabled ) {

			ballDrag.update(camera.camera, ball.position);
			ballDrag.render(Gdx.gl10);
		}	
	}

	@Override
	public void resize(int width, int height) 
	{
		Gdx.gl10.glViewport(0, 0, width, height);

		final float ratio = (float) width / (float) height;
		camera = new GameCamera(new PerspectiveCamera(30f, 2f * ratio, 2f));
	}

	private btDefaultMotionState mapMotionState;
	private btRigidBody mapBody;
	
	@Override
	public void show() {
		Gdx.input.setInputProcessor(this);
		
		physics.loadAssets();
		
		map.replaceEntities(entityInstanceFactory);
		mapMesh = map.createIndexedMesh();

		((BallInstance) ball).body.loadPhysics(physics);
		ball.setupPhysics();
		
		mapBodyShape = new btBvhTriangleMeshShape(true, true, mapMesh);
		mapMotionState = new btDefaultMotionState();
		btRigidBodyConstructionInfo mapCI = new btRigidBodyConstructionInfo(0, mapMotionState, mapBodyShape);
		mapBody = new btRigidBody(mapCI);
		mapCI.delete();
		physics.dynamicsWorld.addRigidBody(mapBody);

		entityInstanceFactory.loadAssets();

		ballDrag.loadAssets();
	}

	@Override
	public void hide() {

		physics.dynamicsWorld.removeRigidBody(mapBody);
		mapBody.delete();
		mapMotionState.delete();
		mapBodyShape.delete();
		
		((BallInstance) ball).body.disposePhysics(physics);

		physics.dispose();
		mapMesh.dispose();
		entityInstanceFactory.dispose();

		ballDrag.dispose();
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean keyDown(int keycode) {

		if( keycode == Input.Keys.ESCAPE ) {
			
			// TODO: Switch back to editor...
			marbill.setScreen(null);
		}
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		
		if( !ballDrag.enabled ) {
		
			ballDrag.start(screenX, screenY);
		}
		
		return false;
	}

	private Vector3 shotImpulse = new Vector3();
	
	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {

		if( ballDrag.enabled ) {
		
			ballDrag.stop(screenX, screenY);
			ballDrag.update(camera.camera,  ball.position);
			
			shotImpulse.set(ballDrag.getArrowDir());
			shotImpulse.scl(ballDrag.getArrowLen()); // TODO: Scale
			
			ball.applyImpulse(shotImpulse);
		}
		
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {

		if( ballDrag.enabled ) {
			
			ballDrag.update(screenX, screenY);
		}

		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}
}

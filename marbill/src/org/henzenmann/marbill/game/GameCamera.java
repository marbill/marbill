package org.henzenmann.marbill.game;

import org.henzenmann.marbill.common.Camera;

import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.math.Vector3;

/**
 * 
 * Camera shows overhead view
 * Always follows ball
 * Ball always shown in center of screen
 * Camera needs to zoom out when making wide shots
 * Similar to the original GTA - the faster you go, the more you see
 * Camera needs to stay centered on ball while doing so
 * Some inertia to make camera movement smoother  
 */
public class GameCamera extends Camera {

	public GameCamera(PerspectiveCamera camera) {
		super(camera);
		camera.near = 0.1f;
		camera.far = 100f;
		angle = 89f;
		distance = 10f;
		update();
	}
	
	public void setBallPosition(Vector3 ballPosition)
	{
		position.set(ballPosition);
		update();
	}

}

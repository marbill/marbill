package org.henzenmann.marbill.physics;

import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.bullet.btBroadphaseInterface;
import com.badlogic.gdx.physics.bullet.btCollisionDispatcher;
import com.badlogic.gdx.physics.bullet.btDbvtBroadphase;
import com.badlogic.gdx.physics.bullet.btDefaultCollisionConfiguration;
import com.badlogic.gdx.physics.bullet.btDiscreteDynamicsWorld;
import com.badlogic.gdx.physics.bullet.btSequentialImpulseConstraintSolver;

public class Physics {

	public static final float Gravity = 10f;
	
	public btBroadphaseInterface broadphase;
	public btDefaultCollisionConfiguration collisionConfiguration;
	public btCollisionDispatcher dispatcher;
	public btSequentialImpulseConstraintSolver solver;
	public btDiscreteDynamicsWorld dynamicsWorld;
	
	public void loadAssets()
	{
		broadphase = new btDbvtBroadphase();
		collisionConfiguration = new btDefaultCollisionConfiguration();
		dispatcher = new btCollisionDispatcher(collisionConfiguration);
	    solver = new btSequentialImpulseConstraintSolver();
	    dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher,broadphase,solver,collisionConfiguration);
	    
	    
        dynamicsWorld.setGravity(new Vector3(0f, -Gravity, 0f));
	}
	
	public void dispose()
	{
		// Delete in opposite order of allocation
		
	    dynamicsWorld.delete();
	    solver.delete();
		dispatcher.delete();
		collisionConfiguration.delete();
		broadphase.delete();
	}
 
}

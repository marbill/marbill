package org.henzenmann.marbill.physics;

import org.henzenmann.marbill.common.EntityInstance;

import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.physics.bullet.btCollisionShape;
import com.badlogic.gdx.physics.bullet.btDefaultMotionState;
import com.badlogic.gdx.physics.bullet.btMotionState;
import com.badlogic.gdx.physics.bullet.btRigidBody;
import com.badlogic.gdx.physics.bullet.btRigidBodyConstructionInfo;
import com.badlogic.gdx.physics.bullet.btSphereShape;

public class Body {
	
	private btCollisionShape collisionShape;
	public btMotionState motionState;
	public btRigidBody rigidBody;
	private EntityInstance entityInstance;
	
	public Body(EntityInstance entityInstance)
	{
		this.entityInstance = entityInstance; 
	}
	
	public void loadPhysics(Physics physics)
	{
		collisionShape = new btSphereShape(0.5f);
		motionState = new btDefaultMotionState();
		
		btRigidBodyConstructionInfo ci = new btRigidBodyConstructionInfo(1f, motionState, collisionShape);
		rigidBody = new btRigidBody(ci);
		ci.delete();
		
		physics.dynamicsWorld.addRigidBody(rigidBody);
	}
	
	public void disposePhysics(Physics physics)
	{
		physics.dynamicsWorld.removeRigidBody(rigidBody);
		
		rigidBody.delete();
		motionState.delete();
		collisionShape.delete();
	}
	
	public Matrix4 worldTransform = new Matrix4();
	
	public void updatePhysics()
	{
		rigidBody.getWorldTransform(worldTransform);
		worldTransform.getTranslation(entityInstance.position);
	}
}
